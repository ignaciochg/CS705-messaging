# https://vyper.readthedocs.io/en/latest/types.html#fixed-size-strings
MSG_LEN:  constant(uint256) = 2048
MAILBOX_SIZE: constant(uint256) = 5
PASSWD_LEN: constant(uint256) = 256
PUBKEY_LEN: constant(uint256) = 2048

struct MSG:
   body: string[MSG_LEN]
   isUsed: bool
   senderAddr: address


NewMsg: event({_box: indexed(address), _index: uint256})

mailbox: MSG[MAILBOX_SIZE]

num_msgs: uint256
passwd: string[PASSWD_LEN]
pubKey: string[PUBKEY_LEN]

writeIndex: uint256
readIndex: uint256

@public
def __init__(_passwd: string[PASSWD_LEN], _pubKey: string[PUBKEY_LEN]):
   self.passwd = _passwd
   self.pubKey = _pubKey
   self.num_msgs = 0
   self.writeIndex = 0
   self.readIndex = 0

   
@public
def getMessage(_passwd: string[PASSWD_LEN], index: uint256) -> (string[MSG_LEN], address) :
   if _passwd != self.passwd: return "Wrong password", self
 
   tmp_msg: MSG = self.mailbox[index]
   if self.mailbox[index].isUsed:
      self.num_msgs = self.num_msgs - 1

   self.mailbox[index].isUsed = False 
   return (tmp_msg.body, tmp_msg.senderAddr)



@public
@constant
def slotUsed(_passwd: string[PASSWD_LEN], index: uint256) -> bool :
   if _passwd != self.passwd: return False
   return self.mailbox[index].isUsed

   
@public
def sendMessage(messg: string[MSG_LEN]):
   assert self.num_msgs < MAILBOX_SIZE, "Mailbox FULL"
   
   for i in range(MAILBOX_SIZE):
      if self.mailbox[i].isUsed == False:
         new_msg: MSG = MSG({body: messg, isUsed: True, senderAddr: self})
         self.mailbox[i] = new_msg
         self.num_msgs = self.num_msgs + 1
         #log.NewMsg(self, i)
         break
      else:
         continue


@public
@constant
def getMailboxSize(_passwd: string[PASSWD_LEN]) -> int128:
   if _passwd != self.passwd:
      return -1
   else:
      return MAILBOX_SIZE

@public
@constant
def getNumberUnread(_passwd: string[PASSWD_LEN]) -> uint256:
   if _passwd != self.passwd: 
      return 0
   else:
      return self.num_msgs


@public
@constant
def getPublicKey() -> string[PUBKEY_LEN]:
   return self.pubKey


   
@public
def destroy(_passwd:string[PASSWD_LEN], dest_addr: address):
   if self.passwd == _passwd:
      selfdestruct(dest_addr)
   else:
      assert 1 == 0

@public
@constant
def getIndex(_passwd:string[PASSWD_LEN]) -> (uint256,uint256,uint256):
   if self.passwd == _passwd:
      return (self.readIndex, self.writeIndex, self.num_msgs)
   else:
      return (3,2,1)
