#!/usr/bin/env python3


import sys
import Client
import web3
from Crypto.PublicKey import RSA


def p_help():
   print("send\tsend message")
   print("getall\tget all messages from box")
   print("getkey\tget someones pub key")
   print("setkey\tset own pubkey")
   print("boxsize\tget own box size")
   print("unreadnumb\tprint number of unread messages")
   print("history\tprint the last messages")
   print("destroy\ttransfer funds and destry mailbox")
   print("help\tthis help")

def getBooleanInput(msg="", defaultBool=None):
   i = input(msg).strip().lower()
   if defaultBool != None and i.strip() == "":
      return defaultBool
   while True:
      if i == "y" or i == "yes" or i == "true" or i == "t":
         return True
      elif i == "n" or i == "no" or i == "false" or i == "f":
         return False
      i = input(msg).strip().lower()


# Get web3 provider URL (Ganache/Ethereum)
providerURL = input("Provider URL [http://127.0.0.1:7545]: ") or "http://127.0.0.1:7545"
w3 = web3.Web3(web3.HTTPProvider(providerURL))

use_ganache = getBooleanInput("Use Ganache account[yes]? ", True)
if use_ganache:
   # Get user account index and user account from Ganache / Ethereum
   acIndex = int(input("Account Index / User ID {0, 1}: "))
   assert acIndex in set({0,1}), "Account Index / User ID must be in {0, 1}"
   myAccAddr = w3.eth.accounts[acIndex]
   default_priv = '{}_priv.pem'.format(acIndex)
   default_pub = '{}_pub.pem'.format(acIndex)
else:
   myAccAddr = input("My account address: ")
   default_priv = '{}_priv.pem'.format(myAccAddr)
   default_pub = '{}_pub.pem'.format(myAccAddr)

print("My account:", myAccAddr)


# Get public private keys' filanmes
fpub_name = input("Public key filename [{}]: ".format(default_pub)) or default_pub
fpriv_name = input("Private key filename [{}]: ".format(default_priv)) or default_priv

# Read public and private keys from file 
pub_key = RSA.import_key(open( fpub_name ).read())
priv_key = RSA.import_key(open( fpriv_name ).read())

use_existing_box = getBooleanInput("Use existing mailbox?[no] ", False)

# Create Client Object
if use_existing_box:
   b_addr = input("Mailbox-ID of existing mailbox: ")
   clnt = Client.Client(providerURL, myAccAddr, pub_key, priv_key,
                     password=None, caddr=b_addr, contractN="mailbox.vy")
else:
   clnt = Client.Client(providerURL, myAccAddr, pub_key, priv_key,
                     password=None, caddr=None, contractN="mailbox.vy")


history = list()
# Simulation
while True:
   inp = input("\nCommand: ").strip()
   if inp == "send":
      t = input("To: ").strip()
      msg = input("Body: ").strip()
      tx_hash, tx_receipt = clnt.sendMessage(t, msg)
      #print(tx_hash, tx_receipt)
   elif inp == "getall":
      #i = int(input("Index: ").strip())
      a = clnt.getAll()
      history += a
      for thing in a:
         print(thing)
   elif inp == "getkey":
      t = input("Of [me]: ").strip()
      if t == "" or t == "me":
         t = clnt.getMailboxID()
      print(clnt.getPublicKey(t))
   elif inp == "setkey":
      k = input("Key: ").strip()
      print(clnt.SetPublicKey(k))
   elif inp == "boxsize":
      print(clnt.getMailboxSize())
   elif inp == "unreadnumb":
      print(clnt.getNumberUnread())
   elif inp == "destroy":
      dst_addr = input("Address of recipient of remaining funds: ")
      if clnt.destroy(dst_addr):
         break
   elif inp == "history":
      lst_n = input("Print last n (0=all) [0]: ")
      if lst_n == "":
         lst_n = 0
      try:
         lst_n = int(lst_n)
      except:
         print("not an integer")
         continue
      if lst_n == 0:
         lst_n = len(history)
      for it in history[len(history)-lst_n:]:
         print(it)
   elif inp =="debug":
      clnt.getIndex()
   elif inp == "help":
      p_help()
   elif inp =="exit":
      break
   elif inp == "":
      continue
   else:
      p_help()
