# Dependencies 

1. ```pip install web3``` (https://github.com/ethereum/web3.py)
2. ```pip install vyper``` (https://vyper.readthedocs.io/en/latest/index.html)
3. Ganache (https://www.trufflesuite.com/ganache)

Setup Ganache with ```gas limit = 99999999999```  (Settings --> Chain --> Edit Gas Limit --> Save & Restart)

# Simulation

#### Setup User 1 

Open a new **terminal 1**
```
python user.py
Provider URL [http://127.0.0.1:7545]:
Account Index / User ID {0, 1}: 0
My account: 0x07FAbaa658Cc4045563790ff0B69C49Fb5c72185
Public key filename [userid_pub.pem]:
Public key filename [userid_priv.pem]:
Password:
pass: abc
Creating new contract
Mailbox ID: 0x043925Cd24B53C20f448530b3d0AA1828D435c68
``` 

#### Setup User 2

Open another **terminal 2**
```
python user.py
Provider URL [http://127.0.0.1:7545]:
Account Index / User ID {0, 1}: 1
My account: 0x077cB20D6C7c0B7aebD456b96BE2C8a42509F550
Public key filename [userid_pub.pem]:
Public key filename [userid_priv.pem]:
Password:
pass: def
Creating new contract
Mailbox ID: 0xE4Da2323756461B149b4C313C65dDa1626cDE639
```

#### Send Message from User 1 to User 2

Go to **terminal 1**
```
Command: send
To: 0xE4Da2323756461B149b4C313C65dDa1626cDE639
Body: Hello user 2
```

#### Read Message for User 2

Go to **terminal 2**
```
Command: get
Index: 0
Hello user 2
```



# Resources 
https://medium.com/block-journal/get-started-with-vyper-the-pythonic-ethereum-smart-contract-language-e5e58969087e   
https://vyper.readthedocs.io/en/latest/built-in-functions.html   
https://web3py.readthedocs.io/en/stable/contracts.html#contract-deployment-example   
   
