#!/usr/bin/env python3 


import sys
import getpass
import web3, vyper

import io, logging, pdb
from Crypto.PublicKey.RSA import RsaKey
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA256

def compile(filePath:str):
   with open(filePath, "r") as f:
      code = f.read()
      comp = vyper.compiler.compile_code(code, ["abi","bytecode"])
      return comp



class Client:
   def __init__(self, providerURL, myAccAddr, pubKey: RsaKey, privKey: RsaKey,
                password=None, caddr=None, contractN="mailbox.vy"): # caddr contract id

      self.w3 = web3.Web3(web3.HTTPProvider(providerURL, request_kwargs={'timeout': 10}))

      self.accAddr = myAccAddr
      self.pubKey = pubKey
      self.privKey = privKey
      
      compiled_c = compile(contractN)
      self.abi = compiled_c["abi"]
      self.bytecode = compiled_c["bytecode"]
      
      if password:
         self.passwd = password
      elif caddr:
         self.passwd = getpass.getpass("Password of {}: ".format(caddr))
      else:
         self.passwd = getpass.getpass("New password: ")

      # print("pass:",self.passwd)

      if caddr:
         print("Using existing contract")
         self.caddr = caddr
         self.contract = self.getContract(caddr, self.abi)
      else:
         print("Creating new contract")
         args = [self.passwd, self.pubKey.exportKey()]
         c, tx_hash, tx_receipt = self.createContract(self.abi, self.bytecode, *args)
         self.contract = self.getContract(tx_receipt.contractAddress,self.abi)
         self.caddr = tx_receipt.contractAddress

      print("Mailbox ID:", self.caddr)

   def call(self, funcName, contract, *args, **kwargs):
      if contract:
         c = contract
      else:
         c = self.contract

      cfunc = getattr(c.functions, funcName)
      return cfunc(*args, **kwargs).call()

   def createContract(self,abi, bytecode, *args):
      contract = self.w3.eth.contract(abi=abi, bytecode=bytecode)
      # deploy
      tx_hash = contract.constructor(*args).transact({'from': self.accAddr})
      # wait for it to get mined and receipt
      tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)

      return contract, tx_hash, tx_receipt

   def decryptMsg(self, cmsg: bytes) -> str:
      if isinstance(cmsg, bytes): cmsg = io.BytesIO(cmsg)

      private_key = self.privKey
      enc_session_key, nonce, tag, ciphertext = [
         cmsg.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1)
      ]

      # Decrypt the session key with the private RSA key
      cipher_rsa = PKCS1_OAEP.new(private_key)
      session_key = cipher_rsa.decrypt(enc_session_key)

      # Decrypt the data with the AES session key
      cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
      msg = cipher_aes.decrypt_and_verify(ciphertext, tag)

      return msg.decode('iso-8859-15')

   def encryptMsg(self, msg : bytes, receiver_pubkey) -> str:
      session_key = get_random_bytes(32)

      # Encrypt the session key with the public RSA key
      cipher_rsa = PKCS1_OAEP.new(receiver_pubkey)
      enc_session_key = cipher_rsa.encrypt(session_key)

      # Encrypt the data with the AES session key
      cipher_aes = AES.new(session_key, AES.MODE_EAX)
      ciphertext, tag = cipher_aes.encrypt_and_digest(msg)
      data = enc_session_key + cipher_aes.nonce + tag + ciphertext

      return data.decode('iso-8859-15')
      

   def getContract(self, addr, abi):
      contract = self.w3.eth.contract(address=addr, abi=abi)
      return contract

   def getMailboxID(self):
      return self.caddr

   def getMailboxSize(self):
      try:
         s = self.call("getMailboxSize", None, self.passwd)
         if s == -1: print("Wrong Password")
      except:
         s = None
      return s
   def getNumberUnread(self):
      try:
         s = self.call("getNumberUnread", None, self.passwd)
         if s == -1: print("Wrong Password")
      except:
         s = None
      return s

   def getPublicKey(self, target_addr):
      c = self.getContract(target_addr, self.abi)
      r = self.call("getPublicKey", c)
      return r

   def getMessage(self, index, encrypted = True):
      try:
         data, sender_addr = self.call("getMessage", None, self.passwd, index)
         
         if data == "Wrong password": 
            print("Wrong password")
            return ""
         elif data == "Empty mailbox":
            print("Empty mailbox")
            return ""
         elif data == "Error":
            print("Error in mailbox, please consider destroying mailbox")
            return ""
         
         if encrypted:
            sender_pubkey = RSA.import_key(self.getPublicKey(sender_addr))
            ciphertext, signature = data[:-256], data[-256:]
            plaintext = self.decryptMsg(ciphertext.encode('iso-8859-15'))
            self.verifyMsg(plaintext, signature, sender_pubkey)
         else:
            plaintext = data 
      except:
         plaintext = None
         logging.error("get message: -- ", exc_info=True)
      return sender_addr, plaintext

   def sendMessage(self, receiver_addr, plaintext):
      try:
         receiver_pubkey = RSA.import_key(self.getPublicKey(receiver_addr))

         plaintext_bytes = plaintext.encode('iso-8859-15')
         ciphertext = self.encryptMsg(plaintext_bytes, receiver_pubkey)
         signature = self.signMsg(plaintext_bytes)
         data = ciphertext + signature
         
         #data = plaintext # delete me
         
         c = self.getContract(receiver_addr, self.abi)
         tx_hash, tx_receipt = self.transactFunc("sendMessage", c, data)
         return tx_hash, tx_receipt

      except:
         logging.error("send message: -- ", exc_info=True)
         return None, None


   def setPublicKey(self, pubkey):
      tx_hash, tx_receipt = self.transactFunc("setPublicKey", None, self.passwd, pubkey)
      return tx_hash, tx_receipt

   def signMsg(self, msg : bytes) -> str:
      key = self.privKey
      h = SHA256.new(msg)
      return pkcs1_15.new(key).sign(h).decode('iso-8859-15')

   def slotUsed(self, index):
      try:
         u = self.call("slotUsed", None, self.passwd, index)
      except:
         u = None
      return u

   def transactFunc(self, funcName, contract, *args, **kwargs):
      if contract:
         c = contract
      else:
         c = self.contract

      cfunc = getattr(c.functions, funcName)
      t_info = {'from': self.accAddr }
      tx_hash = cfunc(*args, **kwargs).transact(t_info)
      tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)

      return tx_hash, tx_receipt

   def verifyMsg(self, plaintext, signature, sender_pubkey):
      h = SHA256.new(plaintext.encode('iso-8859-15'))
      try:
         pkcs1_15.new(sender_pubkey).verify(h, signature.encode('iso-8859-15'))
         return True
      except:
         return False


   def destroy(self, dest_addr):
      try:
         self.call("destroy", None, self.passwd, dest_addr)
         print("Destroyed successfully")
         return True
      except:
         print("Wrong password or mailbox already destroyed")
         return False

   def getIndex(self):
      try:
         print(self.call("getIndex", None,self.passwd))
      except:
         pass

   def getAll(self):
      messages = list()
      for i in range(self.getMailboxSize()):
         try:
            if self.call("slotUsed", None,self.passwd,i):
               messages.append(self.getMessage(i))
         except:
            pass
      return messages